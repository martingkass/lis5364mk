### Martin Kass Presentation:
1. 
    * [MartinKass_LIS5364_Presentation](file:///Users/macowner/lis5364_spr15_presentation_mk14s/MartinKass_LIS5364_Presentation1.ppt)
  
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/adapt" title="1" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/adapt_large" title="2" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/Adaptiveaa.png" title="3" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/enhancement.jpg" title="4" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/finches" title="5" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/gd.png" title="6" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/Graceful.png" title="7" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/nbamobile.png" title="8" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/nbaweb.png" title="9" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/Responsive_ii.png" title="10" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/Responsive-Web-Design-Example- lgbt nationalmuesam_org.png" title="11" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/Responsive-Web-Design-Example-cafeevoke_com.png" title="12" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/Responsive-Web-Design-Example-desk_com.png" title="13" />
    * <img src="file:///Users/macowner/lis5364_spr15_presentation_mk14s/images/yahoo-adaptive.png" title="14" />
    * [README.md](file:///Users/macowner/lis5364_spr15_presentation_mk14s/README.md)
    
References:
Gustafson, Aaron, and Jeffrey Zeldman. Adaptive Web Design: Crafting Rich Experiences with Progressive Enhancement. Chattanooga, Tenn.: Easy Readers, 2011. Print.
Gardner, Brett. "Responsive Web Design: Enriching the User Experience." Sigma E 11.1 (2011): 13-19. Print.
night, Kayla. "Responsive Web Design: What It Is and How to Use It." Smashing E-Book (2011). Smashing Magazine. Web. 17 Feb. 2015. <http://www.smashingmagazine.com/2011/01/12/guidelines-for-responsive-web-design/>.
"Responsive Web Design: 50 Examples and Best Practices - Designmodo." Designmodo. 8 Mar. 2014. Web. 15 Mar. 2015. <http://designmodo.com/responsive-design-examples/>.
"NBA.com, Official Site of the National Basketball Association." NBA.com. Web. 28 Mar. 2015. <http://www.nba.com>.